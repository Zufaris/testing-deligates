﻿namespace testing_Deligates
{
    internal class Program
    {
        private static event Func<Person, int> Age;
        static void Main(string[] args)
        {
            
            Person.OnPerson += University.AddStudent;
            Person.OnAction += Person_OnAction;
            Age += Return_Age;
            char c;
            do
            {
                var men = new Person();
                
                Console.WriteLine("Because you are {0}", Age?.Invoke(men));
                
                c = Console.ReadKey().KeyChar;
                Console.WriteLine();
            } while (c == ' ');
        }

        private static int Return_Age(Person arg)
        {
            return arg.Age;
        }

        private static void Person_OnAction(Person obj)
        {
            string s = "";
            if (obj.Age < 18) s = "You are too young";
                else if (obj.Age > 29) s = "You are too old";
                    else return;
            Console.WriteLine(s);
        }
    }

    class Person
    {
        public static event  EventHandler OnPerson;
        public static event Action<Person> OnAction;
        public string Name { get; set; }
        public int Age { get; set; }
        int id;
        static int count = 0;
        public int Id { get { return id; } set { id = value;} }
        List<string> personNames = new System.Collections.Generic.List<string>() {"Nick", "Ken", "Ben", "Den", "Sam", "Tom" };

        public Person()
        {
            int i = new Random().Next(6);
            Age = new Random().Next(0, 70);
            Id = ++count;
            Name = personNames[i];
            Console.WriteLine(this);
            if (OnPerson != null)
                OnPerson?.Invoke(this, null);
            OnAction?.Invoke(this);
        }

        
        public override string ToString()
        {
            return $"{Name} (id {Id}), age {Age} wants to study in SamGU";
        }
    }

    class University
    {
        public string Name { get; set; }
        public int Id { get; set; }
        public string Faculty { get; private set; }
        List<string> fac = new List<string>() { "Math", "Sociology", "Phisics" };
        

        internal static void AddStudent(object person, EventArgs e)
        {
            if (((Person)person).Age < 30 && ((Person)person).Age > 17)
            {
                var man = new University((Person) person);
            }
        }

        private University(Person person) 
        {
            int i = new Random().Next(3);
            Name = person.Name;
            Faculty = fac[i];
            Id = person.Id;
            Console.WriteLine(this);
        }

        public University()
        {

        }

        public override string ToString()
        {
            return $"Student {Name} accepted on {Faculty} faculty. Id: {Id}";
        }
    }
}